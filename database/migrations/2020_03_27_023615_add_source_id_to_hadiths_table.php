<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSourceIdToHadithsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hadiths', function (Blueprint $table) {
            $table->unsignedBigInteger('source_id')->nullable()->after('my_content');

            $table->foreign('source_id')->references('id')->on('z_sources');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hadiths', function (Blueprint $table) {
            $table->dropForeign(['source_id']);

            $table->dropColumn('source_id');
        });
    }
}
