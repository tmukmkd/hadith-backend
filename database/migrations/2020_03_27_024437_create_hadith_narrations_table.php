<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHadithNarrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hadith_narrations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('hadith_id');
            $table->unsignedBigInteger('narrated_by');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('hadith_id')->references('id')->on('hadiths');
            $table->foreign('narrated_by')->references('id')->on('z_narrators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadith_narrations');
    }
}
