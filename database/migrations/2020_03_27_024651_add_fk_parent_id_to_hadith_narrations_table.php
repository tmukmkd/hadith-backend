<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkParentIdToHadithNarrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hadith_narrations', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('hadith_narrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hadith_narrations', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });
    }
}
