<div class="w-full lg:w-1/2 px-4">
    <div class="bg-white border-t border-b sm:rounded sm:border shadow">
        <div class="border-b">
            <div class="flex justify-between px-6 -mb-px">
                <h3 class="text-blue-dark py-4 font-normal text-lg">Latest Hadith</h3>
            </div>
        </div>
        <div>
            <div class="text-right px-6 py-4">
                {{ $hadith->ar_content }}
            </div>
            <div class="px-6 py-4">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi enim error est eum ex incidunt laboriosam laborum libero minus neque nihil, nisi quas ratione temporibus voluptas. Dolor excepturi magni ut?
                {{ $hadith->my_content }}
            </div>
        </div>
    </div>
</div>
