@extends('layouts.app')

@section('content')
    @foreach($hadiths as $hadith)
        {{nl2br($hadith->ar_content)}} <br><br>
        {!! nl2br($hadith->my_content) !!}
    @endforeach
@endsection
