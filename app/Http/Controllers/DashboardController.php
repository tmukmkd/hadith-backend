<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('dashboard');
    }
}
