<?php

namespace App\View\Components;

use App\Models\Hadith;
use Illuminate\View\Component;

class LatestHadith extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $hadith = Hadith::orderBy('created_at','desc')->first();

        return view('components.latest-hadith', compact('hadith'));
    }
}
