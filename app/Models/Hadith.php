<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Hadith extends Model
{
    use LogsActivity;

    public $fillable = [
        'ar_narration',
        'ar_content',
        'my_narration',
        'my_content',
        'source_id',
    ];
}
